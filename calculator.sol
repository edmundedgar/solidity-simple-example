contract Calculator {

    function addNumbers(uint num1, uint num2) constant returns (uint) {
        return num1 + num2;
    }

}
