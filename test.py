from unittest import TestCase, main
from ethereum import tester as t

class TestCalculator(TestCase):

    def setUp(self):

        s = t.state()
        calculator_code = open('calculator.sol').read()
        self.calculator = t.state().abi_contract(calculator_code, language='solidity', sender=t.k0)

    def test_addition(self):

        two_plus_two = self.calculator.addNumbers(2, 2)
        self.assertEqual( two_plus_two, 4);
    
if __name__ == '__main__':
    main()
